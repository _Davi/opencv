#include"pch.h"
#include<stdio.h>
#include<math.h>
#include<opencv\cv.h>
#include<opencv\highgui.h>
#include<opencv2\objdetect.hpp>
#include<opencv2\highgui.hpp>
#include<opencv2\imgproc.hpp>
#include<vector>

using namespace cv;
using namespace std;

int main(){

	CascadeClassifier face_cascade;
	
	//inserindo o modelo cascade para detecção
	if (!face_cascade.load("C:\\Users\\davi.silva\\Documents\\TesteOpenCv\\haarcascade_frontalface_alt2.xml")) { 
		printf("Error loading cascade file for face");
		return 1;
	}
	
	//inicializando camera
	VideoCapture capture(0); 
	if (!capture.isOpened())
	{
		printf("error to initialize camera");
		return 1;
	}
	
	Mat cap_img, gray_img;
	vector<Rect> faces;
	
	while (1){
		capture >> cap_img;
		waitKey(10);
		
		//transformando a imagem em preto e branco
		cvtColor(cap_img, gray_img, CV_BGR2GRAY); 
		equalizeHist(gray_img, gray_img);
		
		//criando o retângulo que segue o rosto
		face_cascade.detectMultiScale(gray_img, faces, 1.1, 10, CV_HAAR_SCALE_IMAGE | CV_HAAR_DO_CANNY_PRUNING, cvSize(0, 0), cvSize(300, 300));
		for (int i = 0; i < faces.size(); i++)
		{
			Point pt1(faces[i].x + faces[i].width, faces[i].y + faces[i].height);
			Point pt2(faces[i].x, faces[i].y);
			Mat faceROI = gray_img(faces[i]);
						
			rectangle(cap_img, pt1, pt2, cvScalar(0, 255, 0), 2, 8, 0);
		}
		
		//mostrando a imagem
		imshow("Result", cap_img);
		waitKey(3);
		char c = waitKey(3); //fecha quando apertar a tecla esc
		if (c == 27)
			break;
	}
	return 0;
}